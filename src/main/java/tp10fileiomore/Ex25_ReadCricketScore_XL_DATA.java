package tp10fileiomore;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

public class Ex25_ReadCricketScore_XL_DATA {

	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, IOException {
		// TODO Auto-generated method stub
		
		String[][] datafromxl = utils.XLDataReaders.getExcelData("src\\test\\resources\\data\\Cricket_Score_India_Eng_SA.xlsx", "India");
		int nrows = datafromxl.length;
		int ncols = datafromxl[0].length;
		for (int i=0; i < nrows; i++) {
			for( int j = 0; j < ncols; j++) {
				System.out.print(datafromxl[i][j] + "  |  ");
			}
			System.out.println();
		}
		

	}

}
