package tp10fileiomore;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

public class CricketArrayListXLDemo {

	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, IOException {
		// TODO Auto-generated method stub
		
		Player searchPlayer = new Player("MS Dhoni", 	"IN08",	150);
		ArrayList<Player> arrlist = new ArrayList<Player>();
		String fileName = "src\\test\\resources\\data\\Cricket_Score_India_Eng_SA.xlsx";
		String[] sheets = {"India", "England", "SA"};
		

		

		for(int n = 0 ; n < sheets.length; n++) {
			String[][] datafromxl = utils.XLDataReaders.getExcelData(fileName, sheets[n]);

			for(int i = 0; i <datafromxl.length; i++) {
				String Name = datafromxl[i][0];
				String ID = datafromxl[i][1];
				int score = Integer.parseInt(datafromxl[i][2]);

				Player pl = new Player(Name,ID,score);
				arrlist.add(pl);
			}
		}

		for(Player  player      : arrlist) {
			System.out.println(player);
		}
		
		boolean match = false;
		for(Player  player      : arrlist ) {
			String splName = searchPlayer.getName();
			String splID   = searchPlayer.getId();
			int splScore = searchPlayer.getScore();
			
			String plName = player.getName();
			String plID   = player.getId();
			int plScore = player.getScore();
			
			if ( splName.equals(plName)   &&  splID.equals(plID)  &&  splScore == plScore  ) {
				match = true;
				break;
				
			}
		}
		
		if (match == true) {
			System.out.println(searchPlayer + " found in XL Data ");
		} else {
			System.out.println(searchPlayer + " NOT found in XL Data ");
		}
		
		
	}

}
