package tp10fileiomore;

import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.IOException;

public class CSVReaderExample {

    public static void main(String[] args) {

        String csvFile = "src\\main\\resources\\data\\Cricket_Score.csv";

        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader(csvFile),':');
            String[] line;
            while ((line = reader.readNext()) != null) {
                System.out.println("Country [Field1= " + line[0] + ", NAME= " + line[1] + " , SCORE=" + line[2] + "]");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
