package tp10fileiomore;

import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVWriterExample {
    public static void main(String[] args) throws IOException {
        String csv = "src\\test\\resources\\data\\output.csv";
        CSVWriter writer = new CSVWriter(new FileWriter(csv));

        List<String[]> data = new ArrayList<String[]>();
        data.add(new String[] {"India", "New Delhi"});
        data.add(new String[] {"United States", "Washington DC"});
        data.add(new String[] {"Germany", "Berlin"});

        writer.writeAll(data);

        writer.close();
    }
}
