package tp10fileiomore;

public class Player {
	private String name;
	private String id;
	private int score;
	
	
	
	
	public Player(String name, String id, int score) {
		super();
		this.name = name;
		this.id = id;
		this.score = score;
	}




	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}




	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}




	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}




	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}




	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}




	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}




	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Player [name=" + name + ", id=" + id + ", score=" + score + "]";
	}
	
	

}
