package tp03conditional;

public class Ex05_TableOfNumber {
	public static void main(String[] args) {
		int i = 1;
		int num = 7;
		for( ; i <= 10 ; ){   // i++      i = i +1
			System.out.println(num + " * " + i + " = "+ num * i);
			i++;

		}
		
		System.out.println("Outside For Loop " + i);

		i = 1;
		while(i <= 10){
			System.out.println(num + " * " + i + " = "+ num * i);
			i = i + 1;

		}


	}
}
