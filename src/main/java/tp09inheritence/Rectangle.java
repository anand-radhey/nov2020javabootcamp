package tp09inheritence;

public class Rectangle extends Shape {
    double len;
    double width;
    
   @Override
    double calcArea() {
        area = len * width;
        return area;
    }

    public Rectangle() {
    }

    public Rectangle(double len, double width) {
        this.len = len;
        this.width = width;
    }

}
