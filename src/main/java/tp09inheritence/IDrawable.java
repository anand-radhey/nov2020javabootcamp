package tp09inheritence;

public interface IDrawable {
     void draw();
     void erase();
}
