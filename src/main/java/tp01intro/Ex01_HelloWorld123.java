package tp01intro;

import java.util.Scanner;

public class Ex01_HelloWorld123 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/* 
		 * How to use Scanner class 
		 *
		 to take input from
		 System.in
		 */
		Scanner sc = new Scanner(System.in);
		System.out.println("Please give some no.");
		int i = sc.nextInt();
		System.out.println("You have given no. = "+ i);
		
		System.out.println("Hello World ! This is my First Java Program");
		
		

	}

}
