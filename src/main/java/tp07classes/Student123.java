package tp07classes;

public class Student123 {
	// Properties/ Attributes / Fields
	// public   private   default/package   protected
	private int id;   
	private String name;
	private int phy;
	private int chem;
	private int maths;
	
	//Overloading - in the same class
	public Student123() {
		
	}
	
	
	public Student123(int id, String name) {
		this.id = id;
		this.name = name;
		
	}
	
	
	public Student123(int id, String name, int phy, int chem, int maths) {
		super();
		this.id = id;
		this.name = name;
		this.phy = phy;
		this.chem = chem;
		this.maths = maths;
	}


	public int getId() {
		return id;
		
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", phy=" + phy + ", chem=" + chem + ", maths=" + maths + "]";
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getPhy() {
		return phy;
	}


	public void setPhy(int phy) {
		this.phy = phy;
	}


	public int getChem() {
		return chem;
	}


	public void setChem(int chem) {
		this.chem = chem;
	}


	public int getMaths() {
		return maths;
	}


	public void setMaths(int maths) {
		this.maths = maths;
	}


	public void setId(int id) {
		this.id = id;
	}
	
	
	
	

}
