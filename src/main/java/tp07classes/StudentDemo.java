package tp07classes;

import tp05strings.Student;

public class StudentDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student st1 = new Student(10, "Anand");
		System.out.println("st1.roll "+ st1.getRoll() + "   st1.name : "+ st1.getName());
		
		Student st2 = new Student(20, "Rohit");
		System.out.println("st2.roll "+ st2.getRoll() + "   st2.name : "+ st2.getName());

	}

}
