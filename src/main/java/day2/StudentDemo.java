package day2;

import day1.Student;

public class StudentDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student st1 = new Student();
		System.out.println("st1.rollNo = "+ st1.getRoll()  + " st1.name = " + st1.getName());
		
		Student st2 = new Student(102, "Rajesh");
		st2.setName("Rajendra");
		System.out.println("st2.rollNo = "+ st2.getRoll()  + " st2.name = " + st2.getName());
		
		Student st3 = new Student();
		System.out.println("st3.rollNo = "+ st3.getRoll()  + " st3.name = " + st3.getName());
		
	}

}
