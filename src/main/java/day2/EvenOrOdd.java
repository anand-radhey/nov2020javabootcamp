package day2;

import java.util.Scanner;
public class EvenOrOdd {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1, num2, num3 ; //= 27;
		try {

			System.out.println(args[0] + " args[0]  ");
			System.out.println(args[1] + " args[1]  ");
			System.out.println(args[2] + " args[2]  ");
			// Wrapper classes   int -> Integer
			num1 = Integer.parseInt(args[0])   ;  
			num2 = Integer.parseInt(args[1])   ;  
			num3 = Integer.parseInt(args[2])   ;  

			int result = num1 / num2 ;
			System.out.println("num1 / num2 = " + result);

		} catch (ArithmeticException e) {
			// TODO: handle exception
			System.out.println("Inside ArithmeticException catch Block");
			e.printStackTrace();
		} catch (ArrayIndexOutOfBoundsException e) {
			// TODO: handle exception
			System.out.println("Inside ArrayIndexOutOfBoundsException catch Block");
			e.printStackTrace();
		}   
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			System.out.println("Inside finally Block");
		}


/*

		if (   isEven(num1)   ) {
			System.out.println(num1 + " is Even No. ");
		} else {
			System.out.println(num1 + " is Odd No. ");
		}*/



	}


	private static boolean   isEven( int num1){
		int rem = num1 % 2;

		return (rem == 0);
	}







}
