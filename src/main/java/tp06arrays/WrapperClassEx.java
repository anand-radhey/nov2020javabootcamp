package tp06arrays;

public class WrapperClassEx {
    public static void main(String[] args) {
    	//double   -- 
    	//Double dd = 
        int num = 1234321 ;
        //System.out.println(num);
        String str = Integer.toString(num);
        //str = "RADAR";

        StringBuffer sb = new StringBuffer(str);
        String revStr = sb.reverse().toString();

        int revnum = Integer.parseInt(revStr);
        System.out.println(num + " and its reverse is " + revnum);
        
        if(str.equals(revStr)) {
        	System.out.println("Given No. " + num + " is Palindrome" );
        	
        } else {
        	System.out.println("Given No. " + num + " is NOT Palindrome" );
        }
    }
}
