package tp05strings;

public class Student {
	// Attributes   or Fields
	private int roll;
	private String name;
	
	public Student(int roll, String name){
		this.roll = roll;
		this.name = name;
	}

	/**
	 * @return the roll
	 */
	public int getRoll() {
		return roll;
	}

	/**
	 * @param roll the roll to set
	 */
	public void setRoll(int roll) {
		this.roll = roll;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	// getters and setters
	
	
	
	
	
	// Methods

}
